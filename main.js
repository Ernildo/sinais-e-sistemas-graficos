/* Função geradora do gráfico 1 */
const f1 = (x) => {
   let result = 0
   
   if (x <= 20) result = ((4/19)*x) + (15/19)
   else if (x > 20 && x <= 30) result = ((3/(-10))*x) + 11
   else if (x > 30 && x <= 60) result = (1/15)*x
   else result = (1/(-10))*x + 10

   return result
}

/* Função geradora do gráfico 2 */
const f2 = (x) => {
   let result = 0

   if (x <= 40) result = (11/39)*x + 28/39
   else if (x > 40 && x <= 50) result = (7/(-10))*x + 40
   else if (x > 50 && x <= 70) result = x/10
   else result = (3/5)*x - 35

   return result
}

/* Função geradora do gráfico 3 */
const f3 = (x) => {
   let result = 0

   if (x <= 13) result = (19*x - 7)/12
   else if (x > 13 && x <= 30) result = (120/17)*x + 20 - (13*120/17)
   else if (x > 30 && x <= 40) result = (-10)*x + 440
   else result = ((-3)/5)*x + 61

   return result
}

/* dados a serem plotados */
const vetDataC1 = []
for (let i = 1; i <= 90; i++) {
   vetDataC1.push([i, f3(i)])
}


/* função responsável por desenhar o gráfico na página */
function drawChart() {
   
   /* Preparação os dados */
   const data = new google.visualization.arrayToDataTable([
      ['dias', 'pessoas'],
      ...vetDataC1
   ])

   /* Configuração de plot */
   const options = {
      title: 'TERCEIRO CENÉRIO',
      colors: ['red'],
      height: 400,
      width: 600,
      legend: {position: 'none'},
      hAxis: {title: 'Dias',  titleTextStyle: {color: '#333'}},
      vAxis: {title: 'Pessoas',  titleTextStyle: {color: '#333'}},
   }
   const container = document.querySelector('#chart_div')
   const chart1 = new google.visualization.AreaChart(container)
   chart1.draw(data, options)
}

/* Inicialização da biblioteca do google charts */
google.charts.load('current', {packages: ['corechart']})
google.charts.setOnLoadCallback(drawChart)